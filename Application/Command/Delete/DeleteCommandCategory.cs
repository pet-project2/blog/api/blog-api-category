﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Command.Delete
{
    public class DeleteCommandCategory : IRequest<bool>
    {
        public Guid Id { get; set; }
    }
}
