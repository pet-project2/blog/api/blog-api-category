﻿using Common;
using MediatR;
using Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Command.Delete
{
    public class DeleteCommandCategoryHandler : IRequestHandler<DeleteCommandCategory, bool>
    {
        private readonly ICategoryRepository _categoryRepository;
        public DeleteCommandCategoryHandler(ICategoryRepository categoryRepository)
        {
            this._categoryRepository = categoryRepository;
        }

        public async Task<bool> Handle(DeleteCommandCategory request, CancellationToken cancellationToken)
        {
            var category = await this._categoryRepository.GetCategoryByIdAsync(request.Id);
            if(category != null)
            {
                var response = await this._categoryRepository.DeleteCategoryAsync(category);
                if(response > (int)Constants.DatabaseResponse.SaveFailed)
                {
                    return true;
                }
            }

            return false;
        }
    }
}
