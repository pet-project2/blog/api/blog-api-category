﻿using Domain;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Command.Create
{
    public class CreateCommandCategory : IRequest<bool>
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
