﻿using Common;
using Domain;
using Mapster;
using MediatR;
using Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Command.Create
{
    public class CreateCommandCatetoryHandler : IRequestHandler<CreateCommandCategory, bool>
    {
        private readonly ICategoryRepository _categoryRepository; 
        public CreateCommandCatetoryHandler(ICategoryRepository categoryRepository)
        {
            this._categoryRepository = categoryRepository;
        }
        public async Task<bool> Handle(CreateCommandCategory request, CancellationToken cancellationToken)
        {
            var category = request.Adapt<Category>();
            category.Id = Guid.NewGuid();
            var response = await this._categoryRepository.CreateCategoryAsync(category);
            if(response > (int)Constants.DatabaseResponse.SaveFailed)
            {
                return true;
            }

            return false;
        }
    }
}
