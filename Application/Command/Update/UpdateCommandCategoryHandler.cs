﻿using Common;
using MediatR;
using Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Command.Update
{
    public class UpdateCommandCategoryHandler : IRequestHandler<UpdateCommandCategory, bool>
    {
        private readonly ICategoryRepository _categoryRepository;
        public UpdateCommandCategoryHandler(ICategoryRepository categoryRepository)
        {
            this._categoryRepository = categoryRepository;
        }
        public async Task<bool> Handle(UpdateCommandCategory request, CancellationToken cancellationToken)
        {
            var category = await this._categoryRepository.GetCategoryByIdAsync(request.Id);
            if(category != null)
            {
                category.Name = request.Name;
                category.Description = request.Description;

                var response = await this._categoryRepository.UpdateCategoryAsync(category);
                if(response > (int)Constants.DatabaseResponse.SaveFailed)
                {
                    return true;
                }
            }

            return false;
        }
    }
}
