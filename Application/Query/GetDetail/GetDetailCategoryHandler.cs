﻿using Domain;
using MediatR;
using Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Query.GetDetail
{
    public class GetDetailCategoryHandler : IRequestHandler<GetDetailCategory, Category>
    {
        private readonly ICategoryRepository _categoryRepository;
        public GetDetailCategoryHandler(ICategoryRepository categoryRepository)
        {
            this._categoryRepository = categoryRepository;
        }
        public async Task<Category> Handle(GetDetailCategory request, CancellationToken cancellationToken)
        {
            var category = await this._categoryRepository.GetCategoryByIdAsync(request.Id);
            return category;
        }
    }
}
