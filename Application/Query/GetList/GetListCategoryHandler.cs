﻿using Domain;
using MediatR;
using Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Query.GetList
{
    public class GetListCategoryHandler : IRequestHandler<GetListCategory, IEnumerable<Category>>
    {
        private readonly ICategoryRepository _categoryRepository;
        public GetListCategoryHandler(ICategoryRepository categoryRepository)
        {
            this._categoryRepository = categoryRepository;
        }
        public async Task<IEnumerable<Category>> Handle(GetListCategory request, CancellationToken cancellationToken)
        {
            var listCategory = await this._categoryRepository.GetCategoriesAsync();
            return listCategory;
        }
    }
}
