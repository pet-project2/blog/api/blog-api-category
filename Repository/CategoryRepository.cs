﻿using Domain;
using Entity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository
{
    public class CategoryRepository : ICategoryRepository
    {
        private readonly CategoryDbContext _categoryDb;
        public CategoryRepository(CategoryDbContext categoryDb)
        {
            this._categoryDb = categoryDb;
        }

        public async Task<int> CreateCategoryAsync(Category category)
        {
            this._categoryDb.Categories.Add(category);
            var result = await this._categoryDb.SaveChangesAsync();
            return result;
        }

        public async Task<int> DeleteCategoryAsync(Category category)
        {
            this._categoryDb.Categories.Remove(category);
            var result = await this._categoryDb.SaveChangesAsync();
            return result;
        }

        public async Task<IEnumerable<Category>> GetCategoriesAsync()
        {
            var listCategory = await this._categoryDb.Categories.ToListAsync();
            return listCategory;
        }

        public async Task<Category> GetCategoryByIdAsync(Guid id)
        {
            var category = await this._categoryDb.Categories.FindAsync(id);
            return category;
        }

        public async Task<int> UpdateCategoryAsync(Category category)
        {
            this._categoryDb.Categories.Update(category);
            var result = await this._categoryDb.SaveChangesAsync();
            return result;
        }
    }
}
