﻿using Domain;
using Microsoft.EntityFrameworkCore;
using System;

namespace Entity
{
    public class CategoryDbContext : DbContext
    {
        public CategoryDbContext(DbContextOptions<CategoryDbContext> options) : base(options)
        {

        }

        public DbSet<Category> Categories { get; set; }
    }
}
